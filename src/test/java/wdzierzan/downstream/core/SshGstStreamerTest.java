/*
 * Copyright (C) 2012 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.channel.Channel;
import ch.ethz.ssh2.channel.ChannelManager;
import ch.ethz.ssh2.transport.ClientTransportManager;
import ch.ethz.ssh2.transport.TransportManager;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import wdzierzan.downstream.core.Streamer;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.lang.String;
import java.security.SecureRandom;

import static org.junit.Assert.*;

/**
 * @author Wojciech Dzierzanowski (wojciech.dzierzanowski@gmail.com)
 */
public class SshGstStreamerTest {

    private class MockConnection extends Connection {

        private class MockSession extends Session {
            MockSession() throws IOException {
                super(new ChannelManager(new ClientTransportManager(null)) {
                    @Override
                    public Channel openSessionChannel() throws IOException {
                        return null;
                    }
                    @Override
                    public void closeChannel(Channel c, String reason, boolean force) throws IOException {
                    }
                }, null);
            }
            @Override
            public void execCommand(String cmd) throws IOException {
                 ++count;
            }
            @Override
            public InputStream getStdout() {
                return new StringBufferInputStream(stdout[count - 1]);
            }
            @Override
            public InputStream getStderr() {
                return new StringBufferInputStream(stderr[count - 1]);
            }
            @Override
            public int waitForCondition(int condition_set, long timeout) {
                return 0;
            }
        }

        String[] stdout = new String[2];
        String[] stderr = new String[2];
        int count;

        MockConnection() {
            super("dummy");
        }

        @Override
        public synchronized Session openSession() throws IOException {
            return new MockSession();
        }
    }

    private SshGstStreamer streamer;
    private MockConnection connection;

    @Before
    public void newStreamer() {
        connection = new MockConnection();
        streamer = new SshGstStreamer(connection);
    }

    @Test
    public void listEmptyDirectory() throws IOException {
        connection.stdout[0] = connection.stdout[1] = "";
        connection.stderr[0] = connection.stderr[1] = "";
        String[] output = streamer.listFiles("");

        assertArrayEquals(new String[]{}, output);
    }

    @Test
    public void listFile() throws IOException {
        connection.stdout[0] = "";
        connection.stderr[0] = "ls: cannot access file/: Not a directory";
        connection.stdout[1] = "file";
        connection.stderr[1] = "";
        String[] output = streamer.listFiles("file");

        assertNull(output);
    }

    @Test
    public void listFileInDirectory() throws IOException {
        connection.stdout[0] = "";
        connection.stderr[0] = "ls: cannot access dir0/dir00/file000/: Not a directory";
        connection.stdout[1] = "dir0/dir00/file000";
        connection.stderr[1] = "";
        String[] output = streamer.listFiles("dir0/dir00/file000");

        assertNull(output);
    }

    @Test(expected = IOException.class)
    public void listNotExists() throws IOException {
        connection.stdout[0] = connection.stdout[1] = "";
        connection.stderr[0] = "ls: cannot access file/: No such file or directory";
        connection.stderr[1] = "ls: cannot access file: No such file or directory";
        String[] output = streamer.listFiles("file");
    }

    @Test
    public void listNonEmptyDirectory() throws IOException {
        connection.stdout[0] = connection.stdout[1] = "file0\nfile1\nfile2";
        connection.stderr[0] = connection.stderr[1] = "";
        String[] output = streamer.listFiles("dir");

        assertArrayEquals(new String[]{"file0", "file1", "file2"}, output);
    }

    @Test
    public void listDirectoryWithBrokenSymlink() throws IOException {
        connection.stdout[0] = connection.stdout[1] = "file0\nfile1\nfile2";
        connection.stderr[0] = connection.stderr[1] =  "ls: cannot access dir/file1: No such file or directory";
        String[] output = streamer.listFiles("dir");

        assertArrayEquals(new String[]{"file0", "file1", "file2"}, output);
    }

    @Test
    public void listDirectoryWithBrokenSymlinkOnly() throws IOException {
        connection.stdout[0] = connection.stdout[1] = "file1";
        connection.stderr[0] = connection.stderr[1] =  "ls: cannot access dir/file1: No such file or directory";
        String[] output = streamer.listFiles("dir");

        assertArrayEquals(new String[]{"file1"}, output);
    }

    @Test
    public void identifyFlacAudio() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": Audio file with ID3 version 2.3.0, unsynchronized frames, "
                + "contains: FLAC audio bitstream data, 16 bit, stereo, 44.1 kHz, 11919936 samples";
        assertEquals(Streamer.FileType.LOSSLESS_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyFlacAudio2() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": FLAC audio bitstream data, 16 bit, stereo, 44.1 kHz, 7979160 samples";
        assertEquals(Streamer.FileType.LOSSLESS_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyWavAudio() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": RIFF (little-endian) data, WAVE audio, Microsoft PCM, 8 bit, mono 11025 Hz";
        assertEquals(Streamer.FileType.LOSSLESS_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyOggAudio() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": Ogg data, Vorbis audio, stereo, 44100 Hz, ~256000 bps, created by: Xiph.Org libVorbis I";
        assertEquals(Streamer.FileType.LOSSY_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyMp3Audio() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": Audio file with ID3 version 2.3.0, contains: MPEG ADTS, layer III, v1, 192 kbps, 44.1 kHz, JntStereo";
        assertEquals(Streamer.FileType.LOSSY_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyMp3AudioInWav() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": RIFF (little-endian) data, WAVE audio, MPEG Layer 3, stereo 44100 Hz";
        assertEquals(Streamer.FileType.LOSSY_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyASFInWMA() throws IOException {
        String path = "path to file";
        connection.stdout[0] = path + ": Microsoft ASF";
        assertEquals(Streamer.FileType.LOSSY_AUDIO, streamer.identify(path));
    }

    @Test
    public void identifyNoStdout() throws IOException {
        connection.stdout[0] = "";
        assertEquals(Streamer.FileType.UNSTREAMABLE, streamer.identify("path to file"));
    }
}
