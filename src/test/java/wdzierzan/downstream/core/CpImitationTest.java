/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public class CpImitationTest {

    private static final Logger logger = Logger.getLogger(CpImitationTest.class.getName());

    private static class FakeTargetFile implements TargetFile {
        private final File file;
        public FakeTargetFile(File file) {
            this.file = file;
        }
        public File getFile() {
            return file;
        }
        public String getPath() {
            return file.getPath();
        }
        public void commit() {
        }
        public void release() {
        }
    }

    private static class FakeTargetFileFactory implements TargetFileFactory {
        public TargetFile getTargetFile(File file) {
            return new FakeTargetFile(file);
        }
        public TargetFile getTargetFile(String path) {
            return new FakeTargetFile(new File(path));
        }
        public TargetFile getTargetDirectory(File destFile) {
            return new FakeTargetFile(destFile);
        }
    }

    private class MockStreamer implements Streamer, ReceiverRunner {

        private Map<String, String> expectedPaths = new HashMap<String, String>();
        private String lastDestPath;
        private boolean receiverRunning = false;
        private boolean receiverThrows = false;

        private MockStreamer expectFrom(String sourcePath) {
            if (expectedPaths.containsKey(sourcePath))
                throw new IllegalStateException();
            if (expectedPaths.containsValue(null))
                throw new IllegalStateException("'to' and 'from' must intertwine");
            expectedPaths.put(sourcePath, null);
            return this;
        }

        private MockStreamer to(String destPath) {
            if (expectedPaths.containsValue(destPath))
                throw new IllegalStateException();
            for (Map.Entry<String, String> entry : expectedPaths.entrySet())
                if (entry.getValue() == null) {
                    entry.setValue(destPath);
                    return this;
                }
            throw new IllegalStateException("'to' and 'from' must intertwine");
        }

        private MockStreamer willThrowFromReceiver() {
            receiverThrows = true;
            return this;
        }

        public String[] listFiles(String path) throws IOException {
           logger.log(Level.INFO, "path = {0}", path);

            File file = new File(path);
            File[] files = file.listFiles();
            if (files != null) {
                String[] paths = new String[files.length];
                for (int i = 0; i < files.length; i++) {
                    paths[i] = files[i].getPath().substring(path.length() + 1);
                    if (files[i].isDirectory())
                        paths[i] = paths[i] + '/';
                }
                return paths;
            }

            if (!file.exists())
                throw new FileNotFoundException(path);

            return null;
        }

        public FileType identify(String path) throws IOException {
            return FileType.LOSSLESS_AUDIO;
        }

        public void stream(String filePath, FileType ignored) throws IOException {
            filePath = filePath.substring(srcDir.getCanonicalPath().length() + 1);
            String expectedDestPath = expectedPaths.remove(filePath);
            assertEquals(expectedDestPath, lastDestPath);
        }

        public Future<Object> startReceiver(int destPort, final String fullDestPath,
                ProgressReport progressReport) {
            assertFalse(receiverRunning);

            try {
                lastDestPath = fullDestPath.substring(destDir.getRoot().getCanonicalPath().length() + 1);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            FutureTask<Object> receiver = new FutureTask<Object>(new Callable<Object>() {
                public Object call() throws Exception {
                    if (receiverThrows)
                        throw new IOException("Expected failure");
                    receiverRunning = false;
                    return null;
                }
            });
            Thread thread = new Thread(receiver);
            receiverRunning = true;
            thread.start();
            return receiver;
        }

        private void testStream(String sourcePath, String destPath) throws IOException {
            try {
                sourcePath = (new File(srcDir, sourcePath)).getCanonicalPath();
                destPath = (new File(destDir.getRoot(), destPath)).getCanonicalPath();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
                throw new RuntimeException(ex);
            }
            ProgressReport nullReport = new ProgressReport() {
                public void updateProgress(int total, int current) {
                }
                public void updateRate(int kBps) {
                }
                public long getRateUpdatePeriod() {
                    return Long.MAX_VALUE;
                }
            };
            StreamerHandler handler = new StreamerHandler(new FakeTargetFileFactory(), this, nullReport);
            handler.stream(this, 0, sourcePath, destPath, "");

            assertTrue(expectedPaths.isEmpty());
        }
    }

    private static String SRC_DIR = "test-files";
    private static File srcDir;
    private MockStreamer streamer;

    @BeforeClass
    public static void setUpClass() throws Exception {
        URL srcUrl = CpImitationTest.class.getClassLoader().getResource(SRC_DIR);
        srcDir = new File(srcUrl.toURI());
    }

    @Rule
    public TemporaryFolder destDir = new TemporaryFolder();

    @Before
    public void setUp() throws IOException {
        streamer = new MockStreamer();
    }

    @Test
    public void fileToFile() throws IOException {
        streamer.expectFrom("file0").to("file0");
        streamer.testStream("file0", "file0");
    }

    @Test
    public void fileToDir() throws IOException {
        streamer.expectFrom("file0").to("file0");
        streamer.testStream("file0", "");
    }

    @Test
    public void fileInSubSubDirToDir() throws IOException {
        streamer.expectFrom("dir0/dir00/file000").to("file000");
        streamer.testStream("dir0/dir00/file000", "");
    }

    @Test
    public void fileInSubSubDirToFile() throws IOException {
        try {
            File file = new File(destDir.getRoot(), "file000");
            file.createNewFile();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        streamer.expectFrom("dir0/dir00/file000").to("file000");
        streamer.testStream("dir0/dir00/file000", "file000");
    }

    @Test
    public void dirToNewDir() throws IOException {
        streamer.expectFrom("dir0/dir00/file000").to("newdir/file000");
        streamer.testStream("dir0/dir00", "newdir");
    }

    @Test
    public void dirToNewDir2() throws IOException {
        streamer.expectFrom("dir0/file00").to("newdir/file00");
        streamer.expectFrom("dir0/file01").to("newdir/file01");
        streamer.expectFrom("dir0/dir00/file000").to("newdir/dir00/file000");
        streamer.expectFrom("dir0/dir01/file010").to("newdir/dir01/file010");
        streamer.expectFrom("dir0/dir01/file011").to("newdir/dir01/file011");
        streamer.expectFrom("dir0/dir01/file012").to("newdir/dir01/file012");
        streamer.testStream("dir0", "newdir");
    }

    @Test
    public void dirToExistingDir() throws IOException {
        streamer.expectFrom("dir0/file00").to("dir0/file00");
        streamer.expectFrom("dir0/file01").to("dir0/file01");
        streamer.expectFrom("dir0/dir00/file000").to("dir0/dir00/file000");
        streamer.expectFrom("dir0/dir01/file010").to("dir0/dir01/file010");
        streamer.expectFrom("dir0/dir01/file011").to("dir0/dir01/file011");
        streamer.expectFrom("dir0/dir01/file012").to("dir0/dir01/file012");
        streamer.testStream("dir0", "");
    }

    @Test
    public void dirToExistingDirWithSubdirs() throws IOException {
        File subdir = new File(destDir.getRoot(), "dir0/dir00");
        subdir.mkdirs();

        streamer.expectFrom("dir0/file00").to("dir0/file00");
        streamer.expectFrom("dir0/file01").to("dir0/file01");
        streamer.expectFrom("dir0/dir00/file000").to("dir0/dir00/file000");
        streamer.expectFrom("dir0/dir01/file010").to("dir0/dir01/file010");
        streamer.expectFrom("dir0/dir01/file011").to("dir0/dir01/file011");
        streamer.expectFrom("dir0/dir01/file012").to("dir0/dir01/file012");
        streamer.testStream("dir0", "");
    }

    @Test
    public void dirToExistingDirWithFiles() throws IOException {
        File sameFile = new File(destDir.getRoot(), "dir0/dir00/file0");
        File sameDir = sameFile.getParentFile();
        sameDir.mkdirs();
        sameFile.createNewFile();

        streamer.expectFrom("dir0/dir00/file000").to("dir00/file000");
        streamer.testStream("dir0/dir00", "");
    }

    @Test(expected=IOException.class)
    public void dirToFile() throws IOException {
        try {
            File file = new File(destDir.getRoot(), "file0");
            file.createNewFile();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        streamer.testStream("dir0", "file0");
    }

    @Test(expected=IOException.class)
    public void receiverThrows() throws IOException {
        streamer.willThrowFromReceiver();
        streamer.expectFrom("file0").to("file0");
        streamer.testStream("file0", "file0");
    }
}
