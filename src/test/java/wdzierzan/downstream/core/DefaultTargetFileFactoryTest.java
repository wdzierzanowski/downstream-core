/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

public class DefaultTargetFileFactoryTest {
    private static String SRC_DIR = "test-files";
    private static final String DEST_FILE_NAME = "target-file";

    private DefaultTargetFileFactory factory;
    private File destFile;

    @Rule
    public TemporaryFolder destFileParent = new TemporaryFolder();

    @Before
    public void setUp() throws IOException, URISyntaxException {
        destFile = new File(destFileParent.getRoot(), DEST_FILE_NAME);
        factory = new DefaultTargetFileFactory();
    }

    @Test
    public void fileDidNotExistCreatedSuccessfully() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetFile = factory.getTargetFile(destFile);

        targetFile.commit();
        assertThat(targetFile.getFile(), equalTo(destFile));

        targetFile.release();
        assertNull(targetFile.getFile());

        assertTrue(destFile.exists());
    }

    @Test
    public void fileDidNotExistCreatedPartially() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetFile = factory.getTargetFile(destFile);

        Writer writer = new FileWriter(targetFile.getFile());
        try {
            writer.write("partially");
        } finally {
            writer.close();
        }
        assertThat(targetFile.getFile().length(), not(equalTo(0L)));

        // No targetFile.commit()!
        targetFile.release();

        assertNull(targetFile.getFile());
        assertFalse(destFile.exists());
    }

    @Test
    public void fileDidNotExistNotCreated() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetFile = factory.getTargetFile(destFile);

        // No targetFile.commit()!
        targetFile.release();

        assertNull(targetFile.getFile());
        assertFalse(destFile.exists());
    }

    @Test
    public void fileDidNotExistCreatedElsewhereBeforeCommitting() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetFile = factory.getTargetFile(destFile);

        Writer writer = new FileWriter(destFile);
        try {
            writer.write("partially");
        } finally {
            writer.close();
        }
        destFileParent.getRoot().setWritable(false);
        assumeThat(destFile.length(), not(equalTo(0L)));

        IOException thrownException = null;
        try {
            targetFile.commit();
        } catch (IOException e) {
            thrownException = e;
        } finally {
            targetFile.release();
            destFileParent.getRoot().setWritable(true);
        }

        assertNotNull(thrownException);
        assertNull(targetFile.getFile());
        assertThat(destFile.length(), not(equalTo(0L)));
    }

    @Test
    public void fileExistedNewFileCreatedSuccessfully() throws IOException {
        assumeThat(destFile.createNewFile(), is(true));
        assumeThat(destFile.length(), equalTo((0L)));

        TargetFile targetFile = factory.getTargetFile(destFile);
        Writer writer = new FileWriter(targetFile.getFile());
        try {
            writer.write("not empty");
        } finally {
            writer.close();
        }
        assertThat(targetFile.getFile().length(), not(equalTo(0L)));

        targetFile.commit();
        assertThat(targetFile.getFile(), equalTo(destFile));

        targetFile.release();

        assertNull(targetFile.getFile());
        assertThat(destFile.length(), not(equalTo(0L)));
    }

    @Test
    public void fileExistedNewFileCreatedPartially() throws IOException {
        assumeThat(destFile.createNewFile(), is(true));
        assumeThat(destFile.length(), equalTo((0L)));

        TargetFile targetFile = factory.getTargetFile(destFile);
        Writer writer = new FileWriter(targetFile.getFile());
        try {
            writer.write("partially");
        } finally {
            writer.close();
        }
        assertThat(targetFile.getFile().length(), not(equalTo(0L)));

        // No targetFile.commit()!
        targetFile.release();

        assertNull(targetFile.getFile());
        assertTrue(destFile.exists());
        assertThat(destFile.length(), equalTo(0L));
    }

    @Test
    public void fileExistedNewFileNotCreated() throws IOException {
        assumeThat(destFile.createNewFile(), is(true));
        assumeThat(destFile.length(), equalTo((0L)));

        TargetFile targetFile = factory.getTargetFile(destFile);
        // No targetFile.commit()!
        targetFile.release();

        assertNull(targetFile.getFile());
        assertTrue(destFile.exists());
        assertThat(destFile.length(), equalTo((0L)));
    }

    @Test
    public void directoryDidNotExistNothingAddedSuccess() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        targetDir.commit();
        assertThat(targetDir.getFile(), equalTo(destFile));

        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());
    }

    @Test
    public void directoryDidNotExistNothingAddedFailure() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        // No targetDir.commit()!
        targetDir.release();

        assertNull(targetDir.getFile());
        assertFalse(destFile.exists());
    }

    @Test
    public void directoryDidNotExistSomethingAddedSuccess() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child = new File(targetDir.getFile(), "child");
        assumeThat(child.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child);
        targetDirChild.commit();
        targetDirChild.release();

        targetDir.commit();
        assertThat(targetDir.getFile(), equalTo(destFile));

        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0].getName(), equalTo(child.getName()));
    }

    @Test
    public void directoryDidNotExistSomethingAddedFailure() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child = new File(targetDir.getFile(), "child");
        assumeThat(child.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child);
        targetDirChild.commit();
        targetDirChild.release();

        // No targetDir.commit()!
        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0].getName(), equalTo(child.getName()));
    }

    @Test
    public void directoryDidNotExistSomethingFailedToAdd() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child = new File(targetDir.getFile(), "child");
        assumeThat(child.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child);
        // No targetDirChild.commit()!
        targetDirChild.release();

        // No targetDir.commit()!
        targetDir.release();

        assertNull(targetDir.getFile());
        assertFalse(destFile.exists());
    }

    @Test
    public void directoryDidNotExistCreatedElsewhereBeforeCommitting() throws IOException {
        assumeThat(destFile.exists(), not(true));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child = new File(targetDir.getFile(), "child");
        assumeThat(child.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child);
        targetDirChild.commit();
        targetDirChild.release();

        assumeThat(destFile.createNewFile(), is(true));

        File tempFile = targetDir.getFile();
        IOException thrownException = null;
        try {
            targetDir.commit();
        } catch (IOException e) {
            thrownException = e;
        } finally {
            targetDir.release();
        }

        assertNotNull(thrownException);
        assertNull(targetDir.getFile());
        assertFalse(tempFile.exists());
        assertTrue(destFile.exists());
        assertTrue(destFile.isFile());
    }

    @Test
    public void directoryExistedNothingAddedSuccess() throws IOException {
        assumeThat(destFile.mkdir(), is(true));
        assumeThat(destFile.listFiles().length, equalTo((0)));

        TargetFile targetDir = factory.getTargetDirectory(destFile);

        targetDir.commit();
        assertThat(targetDir.getFile(), equalTo(destFile));

        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());
    }

    @Test
    public void directoryExistedWithChildrenNothingAddedSuccess() throws IOException {
        assumeThat(destFile.mkdir(), is(true));
        assumeThat(destFile.listFiles().length, equalTo((0)));
        File child = new File(destFile, "child");
        assumeThat(child.createNewFile(), is(true));

        TargetFile targetDir = factory.getTargetDirectory(destFile);

        targetDir.commit();
        assertThat(targetDir.getFile(), equalTo(destFile));

        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0], equalTo(child));
    }

    @Test
    public void directoryExistedNothingAddedFailure() throws IOException {
        assumeThat(destFile.mkdir(), is(true));
        assumeThat(destFile.listFiles().length, equalTo((0)));
        File child = new File(destFile, "child");
        assumeThat(child.createNewFile(), is(true));

        TargetFile targetDir = factory.getTargetDirectory(destFile);

        // No targetDir.commit()!
        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0], equalTo(child));
    }

    @Test
    public void directoryExistedSomethingAddedSuccess() throws IOException {
        assumeThat(destFile.mkdir(), is(true));
        assumeThat(destFile.listFiles().length, equalTo((0)));
        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child = new File(targetDir.getFile(), "child");
        assumeThat(child.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child);
        targetDirChild.commit();
        targetDirChild.release();

        targetDir.commit();
        assertThat(targetDir.getFile(), equalTo(destFile));

        targetDir.release();
        assertNull(targetDir.getFile());

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0].getName(), equalTo(child.getName()));
    }

    @Test
    public void directoryExistedSomethingFailedToAdd() throws IOException {
        assumeThat(destFile.mkdir(), is(true));
        assumeThat(destFile.listFiles().length, equalTo((0)));
        File child = new File(destFile, "child");
        assumeThat(child.createNewFile(), is(true));

        TargetFile targetDir = factory.getTargetDirectory(destFile);

        File child2 = new File(targetDir.getFile(), "child2");
        assumeThat(child2.exists(), not(true));
        TargetFile targetDirChild = factory.getTargetFile(child2);
        // No targetDirChild.commit()!
        targetDirChild.release();

        // No targetDir.commit()!
        targetDir.release();

        assertTrue(destFile.exists());
        assertTrue(destFile.isDirectory());

        File[] children = destFile.listFiles();
        assumeThat(children, notNullValue());
        assertThat(children.length, equalTo(1));
        assertThat(children[0], equalTo(child));
    }
}
