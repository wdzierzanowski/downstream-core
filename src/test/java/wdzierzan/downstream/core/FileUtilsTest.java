/*
 * Copyright (C) 2011 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileUtilsTest {

    private static String[][] data = {
            {"", "''"},
            {"path", "'path'"},
            {"pa`th", "'pa`th'"},
            {"pa'th", "'pa'\\''th'"},
            {"''path'", "''\\'''\\''path'\\'''"},
    };

    @Test
    public void testEscape() {
        for (int i = 0; i < data.length; i++)
            assertEquals(data[i][1], FileUtils.escape(data[i][0]));
    }
}
