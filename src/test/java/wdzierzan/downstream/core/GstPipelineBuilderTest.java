/*
 * Copyright (C) 2011 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Wojciech Dzierzanowski (wojciech.dzierzanowski@gmail.com)
 */
public class GstPipelineBuilderTest {

    @Test
    public void testNoTranscode() {
        String expectedPipeline =
                "filesrc location='foo' ! tcpclientsink host=bar port=1212 sync=false";
        String pipeline = GstPipelineBuilder.via("bar", 1212).from("foo");
        assertEquals(expectedPipeline, pipeline);
    }

    @Test
    public void testTranscodeToOgg() {
        String expectedPipeline =
                "filesrc location='bar' ! decodebin ! audioconvert "
                + "! vorbisenc quality=0.8 ! oggmux "
                + "! tcpclientsink host=foo port=1212 sync=false";
        String pipeline = GstPipelineBuilder
                .via("foo", 1212)
                .toOggVorbis().quality(0.8)
                .from("bar");
        assertEquals(expectedPipeline, pipeline);
    }

    @Test
    public void testTranscodeToMp3() {
        String expectedPipeline =
                "filesrc location='foo' ! decodebin ! audioconvert "
                + "! lame preset=1002 ! ffmux_mp3 "
                + "! tcpclientsink host=bar port=2121 sync=false";
        String pipeline = GstPipelineBuilder
                .via("bar", 2121)
                .toMp3().preset(1002)
                .from("foo");
        assertEquals(expectedPipeline, pipeline);
    }
}
