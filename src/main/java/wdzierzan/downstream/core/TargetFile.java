/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.io.File;
import java.io.IOException;

/**
 * Represents the target file or directory.
 *
 * For single files, no changes to the filesystem persist beyond the call to
 * {@link #release()} unless {@link #commit()} is called beforehand.
 * Specifically, if TargetFile represents a file, and the file did not exist
 * previously, no file with this path persists.  If the file did exist, it
 * remains intact in its original form.
 *
 * For directories, we do not track operations on individual child paths and
 * we don't treat them as one atomic operation.  So if some files are added to
 * the directory and it is released uncommitted, the files -- and,
 * consequently, the directory itself as well -- persist.  However, if no files
 * are added and the directory did not exist originally, the directory doesn't
 * persist.
 *
 * @note Each TargetFile must be release()-ed when the creator is no longer
 * using it.
 */
public interface TargetFile {
    File getFile();
    String getPath();
    void commit() throws IOException;
    void release();
}
