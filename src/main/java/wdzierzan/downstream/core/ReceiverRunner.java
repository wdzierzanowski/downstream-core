/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.util.concurrent.Future;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public interface ReceiverRunner {

    Future<Object> startReceiver(int destPort, String fullDestPath, ProgressReport progressReport);
}
