/*
 * Copyright (C) 2011 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.io.IOException;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public interface Streamer {

    static enum FileType {
        LOSSLESS_AUDIO, LOSSY_AUDIO, UNSTREAMABLE
    }

    /**
     * @param path directory path
     * @return paths of files and directories in the directory, or
     *      <code>null</code> if <code>path</code> points to a file
     * @throws IOException on error, including when <code>path</code> doesn't
     *      point to a file or directory
     */
    String[] listFiles(String path) throws IOException;

    FileType identify(String path) throws IOException;

    void stream(String path, FileType type) throws IOException;
}
