/*
 * Copyright (C) 2012 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.io.File;
import java.io.FileNotFoundException;

class FileUtils {
    /**
     * Formats a path string for use in a shell command.
     *
     * The path string is potentially altered to make sure it can be used in a
     * shell command without causing wildcard expansion or other side effects.
     *
     * @param path the original path string
     * @return <code>path</code> transformed to something that can be safely
     *      used in a shell command
     */
    public static String escape(String path) {
        return "'" + path.replace("'", "'\\''") + "'";
    }

    public static void deleteRecursively(File file) throws FileNotFoundException {
        if (file.isDirectory())
            for (File child : file.listFiles())
                deleteRecursively(child);

        if (!file.delete())
            throw new FileNotFoundException();
    }
}
