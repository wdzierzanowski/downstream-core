/*
 * Copyright (C) 2011 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public interface ProgressReport {

    /**
     * @param total total number of files to be streamed.  Sometimes, this is
     *      just prediction
     * @param current number of files already streamed
     */
    void updateProgress(int total, int current);

    /**
     * @param kbps kilobytes per second
     */
    void updateRate(int kBps);
    /**
     * @return rate update period in milliseconds
     */
    long getRateUpdatePeriod();
}
