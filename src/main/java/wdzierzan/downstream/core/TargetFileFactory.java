/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.io.File;
import java.io.IOException;

/**
 * @see TargetFile
 */
public interface TargetFileFactory {
    TargetFile getTargetFile(String path) throws IOException;
    TargetFile getTargetFile(File file) throws IOException;
    TargetFile getTargetDirectory(File directory) throws IOException;
}
