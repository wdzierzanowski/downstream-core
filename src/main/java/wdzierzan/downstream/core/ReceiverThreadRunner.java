/*
 * Copyright (C) 2013 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.core;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public class ReceiverThreadRunner implements ReceiverRunner {

    private static final Logger logger = Logger.getLogger(ReceiverThreadRunner.class.getName());

    public Future<Object> startReceiver(final int destPort, final String fullDestPath,
            final ProgressReport progressReport) {
        final FutureTask<Object> future = new FutureTask<Object>(new Callable<Object>() {
            public Object call() throws Exception {
                try {
                    Receiver receiver = new Receiver(destPort);
                    receiver.setProgressReport(progressReport);
                    receiver.saveAs(fullDestPath);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Receiving failed", e);
                    throw e;
                }
                return null;
            }
        });
        Thread thread = new Thread(future);
        thread.start();
        return future;
    }
}
